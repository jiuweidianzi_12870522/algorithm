package org.learn;

import java.util.Arrays;

/**
 * 插入排序
 * 插入排序（Insertion Sort）的基本操作就是将一个数据插入到已经排好序的有序数据中，从而得到一个新的、个数加一的有序数据，算法适用于少量数据的排序，时间复杂度为O(n^2)。是稳定的排序方法。
 *
 * 插入排序的基本思想是：每步将一个待排序的纪录，按其关键码值的大小插入前面已经排序的文件中适当位置上，直到全部插入完为止。
 * ————————————————
 * 版权声明：本文为CSDN博主「fangyaoyu」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
 * 原文链接：https://blog.csdn.net/fangyoayu2013/article/details/50156353
 *
 *
 * 第一步先取出一个元素；
 * 第二步，和前边一个元素比较
 * 比较有两种结果：
 * 结果一：如果取出来的元素比较小，和前一个元素交换位置
 * 结果二：如果取出来的元素比前一个元素大，固定到当前位置
 *
 *
 */
public class InsertionSort {

    public static void main(String[] args) {
        int[] array = new int[]{3, 5, 7, 8, 1, 2, 6, 4, 9};

        int[] ints = insertionSort(array);
        System.out.println(Arrays.toString(ints));
    }

    private static int[] insertionSort(int[] array) {

        return array;
    }

}

package org.learn;

import java.util.Arrays;

/**
 * 选择排序
 * 先执行一次搜索，找到最小数，然后和数组第一位交换位置，
 * 然后继续搜索，找到剩下的数字中的最小数，然后和数组第二位交换位置，
 * ……
 */
public class SelectionSort {

    public static void main(String[] args) {
        int[] array = new int[]{3, 5, 7, 8, 1, 2, 6, 4, 9};

        int[] ints = selectionSort(array);
        System.out.println(Arrays.toString(ints));
    }

    private static int[] selectionSort(int[] array) {

        return array;
    }


}
